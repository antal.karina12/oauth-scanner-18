
package Scan;

import burp.IParameter;
import java.util.List;
import java.util.Set;



final public class ParameterUtilities {


	public static boolean parameterListContainsParameterName(List<IParameter> parameterList, String parameterName) {
		boolean result = false;
		for (IParameter p : parameterList) {
			if (parameterName.equals(p.getName())) {
				result = true;
				break;
			}
		}
		return result;
	}


	public static boolean parameterListContainsParameterName(List<IParameter> parameterList, Set<String> parameterNames) {
		boolean result = false;
		for (IParameter p : parameterList) {
			if (parameterNames.contains(p.getName())) {
				result = true;
				break;
			}
		}
		return result;
	}


	public static IParameter getFirstParameterByName(List<IParameter> parameterList, String parameterName) {
		IParameter result = null;
		for (IParameter p : parameterList) {
			if (parameterName.equals(p.getName())) {
				result = p;
				break;
			}
		}
		return result;
	}
}
