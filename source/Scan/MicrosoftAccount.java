
package Scan;

import burp.IBurpExtenderCallbacks;
import burp.IHttpRequestResponse;
import burp.IParameter;
import burp.IRequestInfo;
import GUI.Logging;
import static Scan.OAuth.ID;
import table.SSOProtocol;
import static table.SSOProtocol.getIDOfLastList;
import static table.SSOProtocol.newProtocolflowID;
import java.util.ArrayList;
import java.util.List;

public class MicrosoftAccount extends SSOProtocol{
    
    public MicrosoftAccount(IHttpRequestResponse message, String protocol, IBurpExtenderCallbacks callbacks){
        super(message, protocol, callbacks);
        super.setToken(findToken());
        super.setProtocolflowID(analyseProtocol());
        add(this, getProtocolflowID());
    }

    @Override
    public int analyseProtocol() {
        logging.log(getClass(), "\nAnalyse: "+getProtocol()+" with ID: "+getToken(), Logging.DEBUG);
        ArrayList<SSOProtocol> last_protocolflow = SSOProtocol.getLastProtocolFlow();
        if(last_protocolflow != null){
            double listsize = (double) last_protocolflow.size();
            double protocol = 0;
            double token = 0;
            
            long tmp = 0;
            long curr_time = 0;
            long last_time = 0;
            boolean wait = true;
            
            for(SSOProtocol sso : last_protocolflow){
                if(sso.getProtocol().contains(this.getProtocol())){
                   logging.log(getClass(), sso.getProtocol(), Logging.DEBUG);
                    protocol++;
                }
                if(sso.getToken().equals(this.getToken())){
                    logging.log(getClass(), sso.getToken(), Logging.DEBUG);
                    token++;
                }
                if(wait){
                    wait = false;
                } else {
                    curr_time = sso.getTimestamp();
                    tmp += curr_time-last_time;
                    logging.log(getClass(), "Diff: "+(curr_time-last_time), Logging.DEBUG);
                }
                last_time = sso.getTimestamp();
            }
            
            if(listsize >= 0){
                double diff_time = ((double)tmp/listsize);
                double curr_diff_time = getTimestamp() - last_protocolflow.get(last_protocolflow.size()-1).getTimestamp();
                double time_bonus = 0;
                logging.log(getClass(), "CurrDiff:"+curr_diff_time+" Diff:"+diff_time, Logging.DEBUG);
                if(curr_diff_time <= (diff_time+4000)){
                    time_bonus = 0.35;
                }
                double prob = ((protocol/listsize)+(token/listsize)*2)/3+(time_bonus);
                logging.log(getClass(), "Probability: "+prob, Logging.DEBUG);
                if(prob >= 0.7){
                    return getIDOfLastList();
                }
            }
            
        }
        return newProtocolflowID();
    }

    @Override
    public String decode(String input) {
        return getHelpers().urlDecode(input);
    }

    @Override
    public String findToken() {
        IRequestInfo iri = super.getCallbacks().getHelpers().analyzeRequest(getMessage());
        List<IParameter> list = iri.getParameters();
        for(IParameter p : list){
            if(p.getName().equals(ID)){
                return decode(p.getValue());
            }
        }
        return "Not Found!";
    }
    
}
