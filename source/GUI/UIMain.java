
package GUI;

import burp.IBurpExtenderCallbacks;
import javax.swing.JTabbedPane;

/**
 * The main window, the parent window for all tabs.
 * @author Tim Guenther
 * @version 1.0
 */
public class UIMain extends JTabbedPane{
    private IBurpExtenderCallbacks callbacks;
    
    //GUI
    private UIHistory history;
    //private UIOptions options;
    //private UIHelp help;

    /**
     * Construct the main UI.
     * @param callbacks {@link burp.IBurpExtenderCallbacks}.
     */
    public UIMain(IBurpExtenderCallbacks callbacks) {
        this.callbacks = callbacks;
        initComponents();
        
    }
    /**
     * 
     * @return Get the history tab.
     */
    public UIHistory getHistory(){
        return history;
    }

    /**
     * 
     * @return Get the help tab.
     */
  /*  public UIHelp getHelp() {
        return help;
    }

    /**
     * 
     * @return Get the options tab. 
     */
   /* public UIOptions getOptions() {
        return options;
    }*/
    
    private void initComponents(){
        //register all components on the extension tab
        //sso history
        history = new UIHistory(callbacks);
        //options
        //options = new UIOptions(callbacks);
        //help
        //help = new UIHelp();

        this.addTab("Oauth History", history);
        //this.addTab("Options", options);
        //this.addTab("Help", help);

        // customize ui components
        callbacks.customizeUiComponent(this);
    }   
}
